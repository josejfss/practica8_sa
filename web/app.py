from flask import Flask
from pymongo import MongoClient

app = Flask(__name__)

@app.route('/')
def hello():
    # Conectar a la base de datos MongoDB
    client = MongoClient('mongodb://db:27017/')

    # Seleccionar la base de datos y la colección
    db = client['practica8']
    collection = db['prueba']

    # Realizar la consulta
    result = collection.find()

    # Presentar la información
    response = ""
    for doc in result:
        response += str(doc) + "\n"

    # Cerrar la conexión a la base de datos
    client.close()

    return response

@app.route('/api/documentos', methods=['GET'])
def crear_documento():
    # Obtener los datos del documento desde la solicitud
    #datos_documento = request.get_json()
    client = MongoClient('mongodb://db:27017/')
    # Acceder a la colección y agregar el documento
    db = client['practica8']
    coleccion = db['prueba']
    documento_id = coleccion.insert_one({'hola':'hola'}).inserted_id

    # Retornar la respuesta como JSON
    respuesta = {'documento_id': str(documento_id)}
    return 'correcto', 201

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80)
